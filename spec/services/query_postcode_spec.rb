# frozen_string_literal: true

require_relative '../../app/services/query_postcode'

RSpec.describe Services::QueryPostcode do
  subject(:get_postcode_info) { described_class.new(postcode: postcode).call }

  let(:postcode) { 'E17QD' }

  before do
    allow(HTTParty)
      .to receive(:get)
      .and_return(api_response)
  end

  describe '.call' do
    let(:api_response) do
      {
        status: 200,
        result: {
          postcode: 'SE1 7QD',
          lsoa: 'Southwark 034A'
        }
      }
    end

    context 'when the request is successful' do
      it 'successfully returns location information' do
        expect(get_postcode_info).to eq api_response[:result]
      end
    end

    context 'when the request is not successful' do
      let(:api_response) { { status: 404, error: 'Invalid postcode' } }

      it 'raises an error with failure details' do
        expect { get_postcode_info }.to raise_error(Services::QueryPostcode::PostcodeApiError, 'Invalid postcode')
      end
    end
  end
end
