# frozen_string_literal: true

require_relative '../../app/services/query_postcode'
require_relative '../../app/services/match_postcode_lsoa'

RSpec.describe Services::MatchPostcodeLsoa do
  subject(:postcode_matcher) { described_class.new(postcode: postcode).call }

  let(:postcode) { 'E1 7QD ' }
  let(:postcode_service) { Services::QueryPostcode }
  let(:postcode_service_instance) { instance_double(postcode_service) }

  describe '.call' do
    let(:postcode_service_response) { { postcode: 'SE17QD', lsoa: lsoa } }
    let(:lsoa) { 'Southwark 034A' }

    before do
      allow(postcode_service).to receive(:new).and_return(postcode_service_instance)
      allow(postcode_service_instance).to receive(:call).and_return(postcode_service_response)
    end

    it 'removes spaces from the postcode' do
      postcode_matcher

      expect(postcode_service)
        .to have_received(:new)
        .with({ postcode: 'E17QD' })
    end

    context 'when postcode is allowed' do
      it 'returns true' do
        expect(postcode_matcher).to eq true
      end

      it 'instantiates the postcode service' do
        postcode_matcher

        expect(postcode_service).to have_received(:new)
      end
    end

    context 'when postcode is not allowed' do
      let(:lsoa) { 'Newham 012A' }

      it 'returns false' do
        expect(postcode_matcher).to eq false
      end
    end

    context 'when postcode is whitelisted' do
      let(:postcode) { 'SH24 1AA' }

      it 'does not instantiate the postcode service' do
        postcode_matcher

        expect(postcode_service).not_to have_received(:new)
      end

      it 'returns true' do
        expect(postcode_matcher).to eq true
      end
    end

    context 'when postcode is not whitelisted' do
      let(:postcode) { 'SH24 1AZ' }

      it 'does not instantiate the postcode service' do
        postcode_matcher

        expect(postcode_service).to have_received(:new)
      end
    end

    context 'when the query postcode service raises an error' do
      before do
        allow(postcode_service_instance)
          .to receive(:call)
          .and_raise Services::QueryPostcode::PostcodeApiError, 'an error occurred'
      end

      it 'rescues and raises the error' do
        expect { postcode_matcher }
          .to raise_error Services::MatchPostcodeLsoa::PostcodeMatcherError, 'an error occurred'
      end
    end
  end
end
