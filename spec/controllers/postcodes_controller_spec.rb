# frozen_string_literal: true

RSpec.describe PostcodesController, type: :controller do
  let(:matcher) { Services::MatchPostcodeLsoa }
  let(:matcher_instance) { instance_double(matcher) }

  before do
    allow(matcher).to receive(:new).and_return(matcher_instance)
    allow(matcher_instance).to receive(:call).and_return true
  end

  describe 'GET /' do
    it 'instantiates the matcher service' do
      get :show, params: { postcode: 'A1 2BC' }

      expect(matcher)
        .to have_received(:new)
        .with({ postcode: 'A1 2BC' })
    end

    it 'responds with the correct data' do
      get :show, params: { postcode: 'A1 2BC' }

      aggregate_failures do
        expect(JSON.parse(response.body)['allowed']).to eq true
        expect(response).to have_http_status :ok
      end
    end

    context 'when the matcher service raises a PostcodeMatcherError' do
      before do
        allow(matcher_instance)
          .to receive(:call)
          .and_raise(Services::MatchPostcodeLsoa::PostcodeMatcherError, 'something happened')
      end

      it 'handles the error' do
        get :show, params: { postcode: 'A1 2BC' }

        aggregate_failures do
          expect(JSON.parse(response.body)['error']).to eq 'Something went wrong, please try again'
          expect(response).to have_http_status :bad_request
        end
      end
    end
  end
end
