# frozen_string_literal: true

RSpec.describe 'PostcodeChecker', type: :system, js: true do
  before do
    visit '/'
  end

  describe 'Served postcode that is whitelisted' do
    it 'informs user of allowed postcode' do
      fill_in 'postcode', with: 'SH24 1AA'
      click_on 'Verify'

      expect(page).to have_text('Postcode is allowed')
    end
  end

  describe 'Served postcode that is not whitelisted' do
    it 'informs user of allowed postcode' do
      fill_in 'postcode', with: 'SW2 5BN'
      click_on 'Verify'

      expect(page).to have_text('Postcode is allowed')
    end
  end

  describe 'Postcode in a location not served' do
    it 'informs user of allowed postcode' do
      fill_in 'postcode', with: 'ME2 3AX'
      click_on 'Verify'

      expect(page).to have_text('Postcode is not allowed')
    end
  end
end
