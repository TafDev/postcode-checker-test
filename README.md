# Postcode Checker

## How to run the app

- cd sh_postcode_checker
- run `bundle install`
- run command `rails s -p 4000`
- open browser and navigate to localhost:4000

## How to run specs

- Make sure you have run `bundle install`
- run command `rspec` or `bundle exec rspec`
- you should see a total of 16 passing examples

### Notes

- Whitelisted postcodes and serviceable areas can potentially be persisted as the list grows instead of being stored in constants
- End to end specs could be improved (have not done these in years)
- As it stand this app does not need the power of rails and could be done in sinatra as a standalone service, Rails was chosen for familiarity
