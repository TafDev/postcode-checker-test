# frozen_string_literal: true

require_relative 'query_postcode'

module Services
  class MatchPostcodeLsoa
    class Error < StandardError; end
    class PostcodeMatcherError < Error; end

    PERMITTED_LSOAS = %w[Southwark Lambeth].freeze
    WHITELISTED_POSTCODES = %w[SH241AA SH241AB].freeze

    def initialize(postcode:)
      @postcode = postcode.gsub(/\s/, '')
    end

    attr_accessor :postcode

    def call
      return true if whitelisted_postcode?

      postcode_allowed?
    rescue Services::QueryPostcode::PostcodeApiError
      raise PostcodeMatcherError, 'an error occurred'
    end

    private

    def postcode_allowed?
      borough_name = postcode_info[:lsoa].split[0...-1].join(' ')

      PERMITTED_LSOAS.include?(borough_name)
    end

    def whitelisted_postcode?
      WHITELISTED_POSTCODES.include?(postcode)
    end

    def postcode_info
      @postcode_info ||= Services::QueryPostcode.new(postcode: postcode).call
    end
  end
end
