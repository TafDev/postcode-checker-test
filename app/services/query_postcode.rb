# frozen_string_literal: true

require 'httparty'

module Services
  class QueryPostcode
    class Error < StandardError; end
    class PostcodeApiError < Error; end

    include HTTParty

    def initialize(postcode:)
      @postcode = postcode
    end

    attr_accessor :postcode

    def call
      query_api
    end

    private

    def query_api
      raise PostcodeApiError, 'Invalid postcode' if response_hash[:status] != 200

      response_hash[:result]
    end

    def response_hash
      response.deep_symbolize_keys
    end

    def response
      @response ||= ::HTTParty.get("http://postcodes.io/postcodes/#{postcode}")
    end
  end
end
