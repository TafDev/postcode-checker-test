# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery unless: -> { request.format.json? && Rails.env.test? } # current workaround for e2e specs
end
