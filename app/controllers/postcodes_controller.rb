# frozen_string_literal: true

require_relative '../../app/services/match_postcode_lsoa'

class PostcodesController < ApplicationController
  def show
    matcher = ::Services::MatchPostcodeLsoa.new(postcode: params[:postcode]).call
    render json: { allowed: matcher }, status: :ok
  rescue Services::MatchPostcodeLsoa::PostcodeMatcherError
    render json: { error: 'Something went wrong, please try again' }, status: :bad_request
  end
end
